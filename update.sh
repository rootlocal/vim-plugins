#!/bin/sh

echo "post-update-cmd script\n"
git submodule init
git submodule foreach "(git checkout master; git pull)&"
#vim +slient +VimEnter +PluginUpdate +qall > /dev/null
echo "complete\n"
